import Config from './config.js'
import JSEncrypt from 'jsencrypt'
import Util from './util.js'
import store from '@/store/index.js'

// #ifdef H5
const app_type = Util.isWeiXin() ? 'wechat' : 'h5';
const app_type_name = Util.isWeiXin() ? '微信公众号' : 'H5';
// #endif

// #ifdef MP-WEIXIN
const app_type = 'weapp';
const app_type_name = '微信小程序';
// #endif

// #ifdef MP-ALIPAY
const app_type = 'aliapp';
const app_type_name = '支付宝小程序';
// #endif

// #ifdef MP-BAIDU
const app_type = 'baiduapp';
const app_type_name = '百度小程序';
// #endif

// #ifdef MP-TOUTIAO
const app_type = 'MP-TOUTIAO';
const app_type_name = '头条小程序';
// #endif

// #ifdef MP-QQ
const app_type = 'MP-QQ';
const app_type_name = 'QQ小程序';
// #endif

// #ifdef APP-PLUS
const app_type = 'app';
const app_type_name = 'APP';
// #endif

export default {
	sendRequest(params) {
		// var method = params.data != undefined ? 'POST' : 'GET', // 请求方式
		var method = 'POST', // 请求方式

			url = Config.baseUrl + params.url, // 请求路径
			data = {
				app_type,
				app_type_name
			};


		//这个判断你是 内存中有token有赋值没有 就不赋值
		if (uni.getStorageSync('token')) data.token = uni.getStorageSync('token');

	 




		if (params.method) {
			method = 'GET';
			if (params.data) {
				url += "?"
				Object.keys(params.data).forEach(key => {
					url += "&" + key + '=' + params.data[key]
				})
			}
		}

		if (params.type == 'store') { //门店端
			params.header.authorize_token = uni.getStorageSync('authorizeToken') || '';
			// data.authorize_token = uni.getStorageSync('token') ||'';
		} else { //用户端
			// token
			if (uni.getStorageSync('token')) data.token = uni.getStorageSync('token');
			var pages = getCurrentPages();
			var page = pages[pages.length - 1];
			var pageRoute = ""
			if (page && page.route) pageRoute = page.route
			// 门店id
			if (uni.getStorageSync('store')) {
				data.store_id = uni.getStorageSync('store').store_id;
			} else if (pageRoute && pageRoute.indexOf('storelist/storelist') == -1 && pageRoute.indexOf(
					'pages/index/index/index') == -1 && pageRoute.indexOf('pages/login/login/login') == -1) {
				uni.setStorageSync('backpage', '/' + pageRoute)
				Util.redirectTo('/otherpages/index/storelist/storelist', page.options, 'redirectTo');
			}
		}

		// 参数
		if (params.data != undefined) Object.assign(data, params.data);

		if (params.async === false) {
			//同步
			return new Promise((resolve, reject) => {
				uni.request({
					url: url,
					method: method,
					data: data,
					header: params.header || {
						'content-type': 'application/x-www-form-urlencoded;application/json'
					},
					dataType: params.dataType || 'json',
					responseType: params.responseType || 'text',
					success: (res) => {
						if (res.data.code == -3 && store.state.siteState > 0) {
							store.commit('setSiteState', -3)
							Util.redirectTo('/pages/storeclose/storeclose/storeclose', {},
								'reLaunch');
							return;
						}
						if (res.data.refreshtoken) {
							uni.setStorage({
								key: 'token',
								data: res.data.refreshtoken
							});
						}
						if (res.data.code == -10009 || res.data.code == -10010) {
							uni.removeStorage({
								key: 'token'
							})
						}
						resolve(res.data);
					},
					fail: (res) => {
						reject(res);
					},
					complete: (res) => {
						reject(res);
					}
				});
			});
		} else {
			//异步
			uni.request({
				url: url,
				method: method,
				data: data,
				header: params.header || {
					'content-type': 'application/x-www-form-urlencoded;application/json'
				},
				dataType: params.dataType || 'json',
				responseType: params.responseType || 'text',
				success: (res) => {
					if (res.data.code == -3 && store.state.siteState > 0) {
						store.commit('setSiteState', -3)
						Util.redirectTo('/pages/storeclose/storeclose/storeclose', {}, 'reLaunch');
						return;
					}
					if (res.data.refreshtoken) {
						uni.setStorage({
							key: 'token',
							data: res.data.refreshtoken
						});
					}
					if (res.data.code == -10009 || res.data.code == -10010) {
						uni.removeStorage({
							key: 'token'
						})
					}
					typeof params.success == 'function' && params.success(res.data);

				},
				fail: (res) => {
					typeof params.fail == 'function' && params.fail(res);
				},
				complete: (res) => {
					typeof params.complete == 'function' && params.complete(res);
				}
			});
		}
	}
}
