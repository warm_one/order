
var config = {
 		// api请求地址
 		baseUrl: process.uniEnv.BASE_API,	
 		// // 图片域名
 		imgDomain: process.uniEnv.BASE_API,
 		// // H5端域名
 		h5Domain: process.uniEnv.BASE_h5Domain,
 		// 腾讯地图key
 		mpKey: process.uniEnv.BASE_mpKey,
 		//客服地址
 		webSocket: '',
 		//本地端主动给服务器ping的时间, 0 则不开启 , 单位秒
 		pingInterval: 1500
 	};
 
 	export default config;
 
