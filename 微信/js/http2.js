// 改写http请求加入头部token

import Config from './config.js'
import JSEncrypt from 'jsencrypt'
import Util from './uti1l.js'
import store from '@/store/index.js'

// #ifdef H5
const app_type = Util.isWeiXin() ? 'wechat' : 'h5';
const app_type_name = Util.isWeiXin() ? '微信公众号' : 'H5';
// #endif

// #ifdef MP-WEIXIN
const app_type = 'wechat';
const app_type_name = '微信小程序';
// #endif

// #ifdef MP-ALIPAY
const app_type = 'aliapp';
const app_type_name = '支付宝小程序';
// #endif

// #ifdef MP-BAIDU
const app_type = 'baiduapp';
const app_type_name = '百度小程序';
// #endif

// #ifdef MP-TOUTIAO
const app_type = 'MP-TOUTIAO';
const app_type_name = '头条小程序';
// #endif

// #ifdef MP-QQ
const app_type = 'MP-QQ';
const app_type_name = 'QQ小程序';
// #endif

// #ifdef APP-PLUS
const app_type = 'app';
const app_type_name = 'APP';
// #endif

export default {
	sendRequest(params) {
		params.method = params.method ? params.method.toUpperCase() : 'POST'; // 请求方式
		// params.method = 'GET'
		let url = Config.baseUrl + params.url; // 请求路径


		let data = {
			app_type,
			app_type_name,
		};
		// uni.showToast({
		// 	title: JSON.stringify(data),
		// 	icon: "none",
		// 	duration: 3000
		// })
		if (!params.header) {
			params.header = {}
		}
		if (params.type && params.type === 'store') {
			params.header["Authori-zation"] = uni.getStorageSync('authorizeToken') || '';
			// params.data["authorize_token"] = uni.getStorageSync('authorizeToken') || '';
			data = {
				...data,
				...params.data
			}
		} else {
			// staff 
			data = {
				...data,
				uid: uni.getStorageSync('uid') || '',
				store_id: uni.getStorageSync('store_id') || '',
				site_id: uni.getStorageSync('site_id') || '',
				token: uni.getStorageSync('token') || '',
				...params.data
			}
		}

		if (params.method === 'GET') {
			if (Object.keys(data).length) {
				url += "?"
				Object.keys(data).forEach(key => {
					url += "&" + key + '=' + data[key]
				})
				params.data = {}
			}
		} else {
			//post 请求 
			params.data = data
		}
		// 参数


		return new Promise((resolve, reject) => {
			uni.request({
				url: url,
				method: params.method,
				data: params.data,
				header: {
					'X-Requested-With': 'XMLHttpRequest',
					"content-type": "application/x-www-form-urlencoded;",
					...params.header,
				},
				dataType: params.dataType || 'json',
				responseType: params.responseType || 'text',
				success: (res) => {
					// console.log('success', res.data)
					params.success && params.success(res.data)
					resolve(res.data);
				},
				fail: (res) => {
					params.fail && params.fail(res.data)
					reject(res);
				},
				complete: (res) => {
					params.complete && params.complete(res.data)
				}
			});
		});
	}
}
