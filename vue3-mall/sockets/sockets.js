import Vue from 'vue';
import VueSocketIo from 'vue-socket.io';

const vueSocket = new VueSocketIo({
    debug: true,
    connection: 'http://192.168.43.117:3000'
});

Vue.use(vueSocket);
