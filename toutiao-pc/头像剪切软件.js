import "cropperjs/dist/cropper.css";
import Cropper from "cropperjs";

onFileChange() {
        // 处于图片预览
        const file = this.$refs.file;

        const temp = window.URL.createObjectURL(file.files[0]);

        this.previewImage = temp;

        // 展示弹出层，预览用户选择的图片
        this.dialogVisible = true;

        // 解决选择相同文件不触发 change 事件问题
        this.$refs.file.value = "";
    },

    onDialogOpened() {
        // 图片裁切器必须基于 img 进行初始化
        // 注意：img 必须是可见状态才能正常完成初始化
        //       因为我们这里要在对话框里面初始化裁切器
        //       所以务必要在对话框完全打开的状态去进行初始化。
        // 获取图片 DOM 节点
        const image = this.$refs["preview-image"];

        // 第1次初始化好以后，如果预览裁切的图片发生了变化，裁切器默认不会更新
        // 如果需要预览图片发生变化更新裁切器：
        //    方式一：裁切器.replace 方法
        //    方式二：销毁裁切器，重新初始化
        // 初始化裁切器
        if (this.cropper) {
            this.cropper.replace(this.previewImage);
            return;
        }
        this.myCropper = new Cropper(image, {
            viewMode: 1, //裁剪框不可以移出画布
            dragMode: 'move', //图片可移动

            autoCrop: true, // 允许初始化时自动的裁剪图片 配合 data 使用 默认true
            initialAspectRatio: 1 / 1,
            aspectRatio: 1 / 1, //设置裁剪框为固定的宽高比
            autoCropArea: 1, // 设置裁剪区域占图片的大小 值为 0-1 默认 0.8 表示 80%的区域
            background: false, //不在容器内显示网格状的背景
            guides: false, //不显示虚线

            movable: false,
            scalable: false,
            zoomable: false,
            touchDragZoom: false,
            zoomOnWheel: false,
            zoomOnTouch: false,
            mouseWheelZoom: false,
            doubleClickToggle: false,

            // cropBoxMovable: false,// 是否可以拖拽裁剪框 默认true
            // cropBoxResizable: false,// 是否可以改变裁剪框的尺寸 默认true

            toggleDragModeOnDblclick: false, //点击两次时不在“crop”和“move”之间切换拖拽模式，

            minContainerWidth: 355, //容器的最小宽度。。相对页面
            minContainerHeight: 355,
            minCanvasWidth: 355, //画布最小宽高
            minCanvasHeight: 355,
            // minCropBoxWidth: 355,  //裁剪框的 最小宽高
            // minCropBoxHeight: 355,
            center: false, // 是否显示裁剪框中间的 ‘+’ 指示器 默认true 裁剪框默认在画布中间 
            checkOrientation: true, //检查图片方向值
            checkImageOrigin: true,
            crop() {
                // this.cropper.cropstart()
                afterImgDataUrl = this.cropper.getCroppedCanvas({ //getCroppedCanvas([options]) 得到被裁剪图片的一个canvas对象 options设置这个canvas的一些数据
                    width: 355,
                    height: 355,
                    // minWidth:355,//输出画布的最小目标宽度，默认值为0。
                    // minHeight:355,//输出画布的最小目标高度，默认值为0。
                    // maxWidth:355,//输出画布的最大目标宽度，默认值为Infinity(无穷大)。
                    // maxHeight:355,
                    imageSmoothingQuality: 'high'
                }).toDataURL('image/jpeg', 1)
                that.setState({ dateUrl: afterImgDataUrl })
            },
        })

    }————————————————
版权声明： 本文为CSDN博主「 酸菜鱼爱吃柠檬」 的原创文章， 遵循CC 4.0 BY - SA版权协议， 转载请附上原文出处链接及本声明。
原文链接： https: //blog.csdn.net/weixin_43163869/article/details/109326725
},

onDialogClosed() {
        // 对话框关闭，销毁裁切器
        // this.cropper.destroy()
    },

    onUpdatePhoto() {
        // 让确定按钮开始 loading
        this.updatePhotoLoading = true;
        // 获取裁切的图片对象
        this.cropper.getCroppedCanvas().toBlob(file => {
            const fd = new FormData();
            fd.append("photo", file);
            // 请求更新用户头像请求提交 fd
            updateUserPhoto(fd).then(res => {
                console.log(res);
                // 关闭对话框
                this.dialogVisible = false;
                // 更新视图展示

                // 直接把裁切结果的文件对象转为 blob 数据本地预览
                this.user.photo = window.URL.createObjectURL(file);

                // 关闭确定按钮的 loading
                this.updatePhotoLoading = false;

                this.$message({
                    type: "success",
                    message: "更新头像成功"
                });

                // 更新顶部登录用户的信息
                globalBus.$emit("update-user", this.user);

                // 把服务端返回的图片进行展示有点慢
                // this.user.photo = res.data.data.photo
            });
        });