  ImagePreview  from vant 


created() {
    this.loadArticle()
  },
  methods: {
    async loadArticle() {
      const { data: res } = await getArticleById(this.articleId)
      this.list = res.data
      this.$nextTick(() => {
        this.handlerPreviewImage()
      })
    },
    handlerPreviewImage() {
      const content = this.$refs.contentRef
      const imgs = content.querySelectorAll('img')
      const imgPath = []
      imgs.forEach((item, index) => {
        imgPath.push(item.src)
        item.onclick = function() {
          ImagePreview({
            images: imgPath,
            startPosition: index
          })
        }
      })
    },

加载成功 延迟获取