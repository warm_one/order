.input-add button {
  width: 46px;
  height: 46px;
  border-radius: 50%;
  background: linear-gradient(#c0a5f3, #7f95f7);
  border: none;
  color: white;
  position: absolute;
  right: 0px;

  cursor: pointer;
  outline: none;
}

.input-add .plus {
  display: block;
  width: 100%;
  height: 100%;
  background: linear-gradient(#fff, #fff), linear-gradient(#fff, #fff);
  background-size: 50% 2px, 2px 50%;
  background-position: center;
  background-repeat: no-repeat;
}

.filters span {
  margin-right: 14px;
  transition: 0.8s;
}

.filters .active {
  color: #6b729c;
  transform: scale(1.2);
}
.todo-item label span  {
  position: absolute;
  top: 0;
}

.todo-item label span::before,
.todo-item label span::after {
  content: "";
  display: block;
  position: absolute;
  width: 18px;
  height: 18px;
  border-radius: 50%;
}

.todo-item label span::before {
  border: 1px solid #b382f9;
}

.todo-item label span::after {
  transition: 0.4s;
  background: #b382f9;
  transform: translate(1px, 1px) scale(0.8);
  opacity: 0;
}
.todo-item input {
  margin-right: 16px;
  opacity: 0; 
}
.todo-item input:checked + span::after {
  opacity: 1;
}